<?php

Namespace Jds\Models;

use \PDO;
// use \Exceptions\CouldNotConnectException;

class PDOAdaptor {

	private $db;

	public function __construct($dsn, $username, $password)
	{
		try
		{
			$PDO = new \PDO($dsn, $username, $password);
			$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Set Errorhandling to Exception
			$this->setDb($PDO);
		}
		catch(\PDOException $e)
		{
			//throw new CouldNotConnectException('Er kon geen verbinding met de database gemaakt worden', $e->getMessage() , $e->getCode());
		}
	}

	public function setDb($db)
	{
		$this->db = $db;
	}

	public function getDb()
	{
		return $this->db;
	}
}
