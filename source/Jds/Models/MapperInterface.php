<?php

/**
 * @author Jasper De Smet <jasp.desmet@gmail.com>
 * @version 1.0
 * @package Jds\Models
 */

Namespace Jds\Models;

interface MapperInterface {

	public function getAll();
	public function getById($id);
	public function insert();
	public function delete($id);
	public function update();
}