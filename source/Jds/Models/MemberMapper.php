<?php

/**
 * @author Jasper De Smet <jasp.desmet@gmail.com>
 * @version 1.0
 * @package Jds\Models
 */

Namespace Jds\Models;
use \PDO;

class MemberMapper
{
    private $adaptor;

    public function setAdaptor($adaptor)
    {
        $this->adaptor = $adaptor;
    }


    public function getAdaptor()
    {
        return $this->adaptor;
    }

    public function __construct($adaptor)
    {
        $this->setAdaptor($adaptor);
    }

    /**
     * populates array into a Member object
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   array $data
     * @return  Member
     */
    public function populate($data)
    {
        $member = new Member();

        if(isset($data['memberId']))
        {
            $member->setId($data['memberId']);
        }

        if(isset($data['RFID']))
        {
            $member->setRFID($data['RFID']);
        }
            
        if(isset($data['firstname']))
        {
            $member->setFirstname($data['firstname']);
        }

        if(isset($data['lastname']))
        {
            $member->setLastname($data['lastname']);
        }

        if(isset($data['address']))
        {
            $member->setAdress($data['address']);
        }

        if(isset($data['zipCode']))
        {
            $member->setzipCode($data['zipCode']);
        }

        if(isset($data['city']))
        {
            $member->setCity($data['city']);
        }

        if(isset($data['dateOfBirth']))
        {
            $member->setDateOfBirth($data['dateOfBirth']);
        }

        if(isset($data['GSM']))
        {
            $member->setGSM($data['GSM']);
        }

        if(isset($data['mail']))
        {
            $member->setMail($data['mail']);
        }

        if(isset($data['password']))
        {
            $member->setPassword($data['password'], true);
        }

        if(isset($data['pin']))
        {
            $member->setPin($data['pin']);
        }

        if(isset($data['gender']))
        {
            $member->setGender($data['gender']);
        }

        if(isset($data['memberSince']))
        {
            $member->setMemberSince($data['memberSince']);
        }

        if(isset($data['rfidExpireDate']))
        {
            $member->setRfidExpireDate($data['rfidExpireDate']);
        }

        if(isset($data['image']))
        {
            $member->setImage($data['image']);
        }

        return $member;
    }

    function authenticate($mail, $password, $pemissionId) {

            $sql = "SELECT m.firstName, m.lastName, mr.roleId
                    FROM member as m
                    INNER JOIN memberrole as mr
                    ON mr.memberId = m.memberId
                    INNER JOIN rolepermission as rp
                    ON rp.roleId = mr.roleId
                    WHERE SHA1(m.mail) = :mail
                    AND m.password = :password
                    AND rp.permissionId = :permissionId
                   "; 

        $statement = $this->getAdaptor()->getDb()->prepare($sql);
        $statement->execute(array(':mail' => $mail, ':password' => $password, ':permissionId' => $pemissionId));
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        if(count($results) == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   int $id
     * @return  Member
     */
    function getById($id) {

        $sql = "SELECT m.memberId, m.RFID, m.firstname, m.lastname, m.address, m.zipCode, 
                       m.city, m.dateOfBirth, m.GSM, m.mail, m.password, m.pin, 
                       m.gender, m.memberSince, m.rfidExpireDate, m.image
                FROM member as m
                WHERE m.memberId = :id
               "; 

        $statement = $this->getAdaptor()->getDb()->prepare($sql);
        $statement->execute(array(':id' => $id));
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        if(count($results) == 1) {
            return $this->populate($results[0]);
        }
        else {
            return false;
        }
    }

    /**
     * find duplicated
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   String firstname, String lastname, Date (YYYY-MM-DD) date
     * @return  Member
     */
    public function getByFirstnameLastnameDateOfBirth($firstname, $lastname, $date)
    {
        try {

            $SQL = "SELECT m.memberId, m.RFID, m.firstname, m.lastname, m.address, m.zipCode, 
                       m.city, m.dateOfBirth, m.GSM, m.mail, m.password, m.pin, 
                       m.gender, m.memberSince, m.rfidExpireDate, m.image
                    FROM member as m
                    WHERE dateOfBirth = :dateOfBirth
                    AND firstname = :firstname
                    AND lastname = :lastname";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':dateOfBirth' => $date,
                                      ':firstname' => $firstname,
                                      ':lastname' => $lastname
                ));

            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }

        if(count($results) == 1)
        {
            return $this->populate($results[0]);
        }
        else
        {
            return false;
        }   
    }

    /**
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   int $id
     * @return  Member
     */
    function getAll() {

        $sql = "SELECT m.memberId, m.RFID, m.firstname, m.lastname, m.address, m.zipCode, 
                       m.city, m.dateOfBirth, m.GSM, m.mail, m.password, m.pin, 
                       m.gender, m.memberSince, m.rfidExpireDate, m.image
                FROM member as m
               "; 

        $statement = $this->getAdaptor()->getDb()->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        if(count($results) >= 1) {

            $users = array();

            foreach($results as $result) {
                $users[] = $this->populate($result);
            }

            return $users;
        }
        else {
            return false;
        }
    }

    /**
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   Member $member
     * @return  array $respons
     */
    function create(Member $member) {

        // Check eerst of alle verplichte velden gevuld zijn
        if($member->getFirstname() == '' || $member->getLastname() == '' || $member->getRFID() == '' || $member->getAddress() == '' || $member->getCity() == '' || $member->getDateOfBirth() == '' || $member->getGender() == '') {
            $respons['status'] = 412;
            $respons['message'] = 'Verplichte velden zijn niet ingevuld';

            return $respons;
        }
        else {
            
            $sql = "INSERT IGNORE INTO member
                    VALUES ('',
                            :RFID, 
                            :firstName, 
                            :lastName, 
                            :adress, 
                            :zipCode, 
                            :city, 
                            :dateOfBirth, 
                            :GSM, 
                            :mail, 
                            :password, 
                            :pin, 
                            :gender, 
                            NOW(),
                            :image,
                            NOW() + INTERVAL 1 YEAR
                            )
                    ";

            $statement = $this->getAdaptor()->getDb()->prepare($sql);

            $params = array(':RFID' => $member->getRFID(),
                            ':firstName' => $member->getFirstname(),
                            ':lastName' => $member->getLastname(),
                            ':adress' => $member->getAddress(),
                            ':zipCode' => $member->getzipCode(),
                            ':city' => $member->getCity(),
                            ':dateOfBirth' => $member->getDateOfBirth(),
                            ':GSM' => $member->getGSM(),
                            ':mail' => $member->getMail(),
                            ':password' => $member->getPassword(),
                            ':pin' => $member->getPin(),
                            ':gender' => $member->getGender(),
                            ':image' => $member->getImage()
                );

            $statement->execute($params);
            

            if($statement->rowCount() == 1) {
                $respons['status'] = 201;
                $respons['message'] = 'OK';

                return $respons;
            }
            else {
                $respons['status'] = 409;
                $respons['message'] = 'De RFID kaart die u gebruikt bestaat waarschijnlijk al.';

                return $respons;
            }
        }
    }

    /**
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   Member $member
     * @return  
     */
    function update(Member $member) {

        if($member->getId() != '') {
            // Start SQL
            $sql = "UPDATE member ";

            // parameterBag
            $params = array();

            // Dynamisch opbouwen a.d.h.v. parameters
            if($member->getRfidExpireDate() != '') {
                $sql .= "SET rfidExpireDate = :rfidExpireDate ";
                $params['rfidExpireDate'] = $member->getRfidExpireDate();
            }

            // !!! BELANGRIJK !!! WHERE ID = ID!
            $sql .= "WHERE memberId = :memberId";
            $params['memberId'] = $member->getId();

            $statement = $this->getAdaptor()->getDb()->prepare($sql);

            $statement->execute($params);

            if($statement->rowCount() == 1) {
                $respons['status'] = 200;
                $respons['message'] = 'OK';

                return $respons;
            }
            else {
                $respons['status'] = 500;
                $respons['message'] = 'Member is niet geupdated.';

                return $respons;
            }
        }
        else {
            $respons['status'] = 412;
            $respons['message'] = 'Member.id is leeg.';

            return $respons; 
        }
    }
}