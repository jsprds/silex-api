<?php

/**
 * @author Jasper De Smet <jasp.desmet@gmail.com>
 * @version 1.0
 * @package Jds\Models
 */

namespace Jds\Models;

class Member {

    private $id;
    private $RFID;
    private $firstname;
    private $lastname;
    private $address;
    private $zipCode;
    private $city;
    private $dateOfBirth;
    private $GSM;
    private $mail;
    private $password;
    private $pin;
    private $gender;
    private $memberSince;
    private $rfidExpireDate;
    private $image;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getRFID() {
        return $this->RFID;
    }

    public function setRFID($RFID) {
        $this->RFID = $RFID;
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAdress($address) {
        $this->address = $address;
    }

    public function getZipCode() {
        return $this->zipCode;
    }

    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getDateOfBirth() {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth) {
        $this->dateOfBirth = $dateOfBirth;
    }

    public function getGSM() {
        return $this->GSM;
    }

    public function setGSM($GSM) {
        $this->GSM = $GSM;
    }

    public function getMail() {
        return $this->mail;
    }

    public function setMail($mail) {
        $this->mail = $mail;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPin() {
        return $this->pin;
    }

    public function setPin($pin) {
        $this->pin = $pin;
    }

    public function getGender() {
        return $this->gender;
    }

    public function setGender($gender) {
        if(is_numeric($gender)) {
            $this->gender = $gender;
        }
        elseif($gender == 'M') {
            $this->gender = 1;
        }
        elseif('V') {
            $this->gender = 0;
        }
        else {
            $this->gender = '';
        }
    }

    public function getMemberSince() {
        return $this->memberSince;
    }

    public function setMemberSince($memberSince) {
        $this->memberSince = $memberSince;
    }

    public function getRfidExpireDate() {
        return $this->rfidExpireDate;
    }

    public function setRfidExpireDate($rfidExpireDate) {
        $this->rfidExpireDate = $rfidExpireDate;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getArray() {
        return get_object_vars($this);
    }
}