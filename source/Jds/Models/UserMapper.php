<?php

/**
 * @author Jasper De Smet <jasp.desmet@gmail.com>
 * @version 1.0
 * @package Models\User
 */

Namespace Jds\Models;
use Models\User\User;
use Models\PDOAdaptor;
use \PDO;

require_once('User.php');

class UserMapper
{
    private $adaptor;

    public function setAdaptor($adaptor)
    {
        $this->adaptor = $adaptor;
    }


    public function getAdaptor()
    {
        return $this->adaptor;
    }

    public function __construct()
    {
        $PDOAdaptor = new PDOAdaptor();
        $this->setAdaptor($PDOAdaptor);
    }


    /**
     * populate array into a User object
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   array $data
     * @return  User
     */
    public function populate($data)
    {
        if(isset($data['id']))
        {
            $user = new User();

            if(isset($data['id']))
            {
                $user->setId($data['id']);
            }

            if(isset($data['RFID']))
            {
                $user->setRFID($data['RFID']);
            }

            if(isset($data['firstName']))
            {
                $user->setFirstName($data['firstName']);
            }

            if(isset($data['lastName']))
            {
                $user->setLastName($data['lastName']);
            }

            if(isset($data['adress']))
            {
                $user->setAdress($data['adress']);
            }

            if(isset($data['zipCode']))
            {
                $user->setzipCode($data['zipCode']);
            }

            if(isset($data['city']))
            {
                $user->setCity($data['city']);
            }

            if(isset($data['dateOfBirth']))
            {
                $user->setDateOfBirth($data['dateOfBirth']);
            }

            if(isset($data['GSM']))
            {
                $user->setGSM($data['GSM']);
            }

            if(isset($data['mail']))
            {
                $user->setMail($data['mail']);
            }

            if(isset($data['password']))
            {
                $user->setPassword($data['password'], true);
            }

            if(isset($data['pin']))
            {
                $user->setPin($data['pin']);
            }

            if(isset($data['gender']))
            {
                $user->setGender($data['gender']);
            }

            if(isset($data['memberSince']))
            {
                $user->setMemberSince($data['memberSince']);
            }

            if(isset($data['rfidExpireDate']))
            {
                $user->setRfidExpireDate($data['rfidExpireDate']);
            }

            if(isset($data['image']))
            {
                $user->setImage($data['image']);
            }

            return $user;
        }
        else 
        {
            return false;
        }
    }

     /**
     * find User by id from storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param
     * @return  User
     */
    public function findAll()
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    ";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            throw $e;
        }
        

        if(count($results) >= 1)
        {
            foreach($results as $result) {
                $newUsers[] = $this->populate($result);
            }

            return $newUsers;
           
        }
        else
        {
            return false;
        }   
    }

    /**
     * find User by id from storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findById(User $user)
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    WHERE id = :id";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':id' => $user->getId()));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }
        

        if(count($result) === 1)
        {
            $newUser = $this->populate($result[0]);
            return $newUser;
        }
        else
        {
            return false;
        }   
    }

    /**
     * find User by RFID from storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findByRfid(User $user)
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    WHERE rfid = :rfid";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':rfid' => $user->getRfid()));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }
        

        if(count($result) === 1)
        {
            $newUser = $this->populate($result[0]);
            return $newUser;
        }
        else
        {
            return false;
        }   
    }

    /**
     * find User by mail from storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findByMail(User $user)
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    WHERE mail = :mail";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':mail' => $user->getMail()));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }
        

        if(count($result) === 1)
        {
            $newUser = $this->populate($result[0]);
            return $newUser;
        }
        else
        {
            return false;
        }   
    }

    /**
     * find User by mail and password from storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findByMailAndPass(User $user)
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    WHERE mail = :mail
                    AND password = :password";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':mail' => $user->getMail(), ':password' => $user->getPassword()));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }

        if(count($result) === 1)
        {
            $newUser = $this->populate($result[0]);
            return $newUser;
        }
        else
        {
            return false;
        }   
    }

    /**
     * find duplicated
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findDuplicated(User $user)
    {
        try {

            $SQL = "SELECT COUNT(id) as result
                    FROM users
                    WHERE dateOfBirth = :dateOfBirth
                    AND firstName = :firstName
                    AND lastName = :lastName";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':dateOfBirth' => $user->getDateOfBirth(),
                                      ':firstName' => $user->getFirstName(),
                                      ':lastName' => $user->getLastName()
                ));

            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }

        if($result[0]['result'] == 1)
        {
            // duplicated
            return true;
        }
        else
        {
            return false;
        }   
    }

    /**
     * find User by date of birth
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function findByDateOfBirth(User $user)
    {
        try {

            $SQL = "SELECT id, RFID, firstName, lastName, adress, zipCode, city, dateOfBirth, GSM, mail, password, pin, gender, memberSince, rfidExpireDate, image
                    FROM users
                    WHERE dateOfBirth = :dateOfBirth";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);
            $statement->execute(array(':dateOfBirth' => $user->getDateOfBirth()));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOExeption $e) {
            throw $e;
        }

        if(count($result) === 1)
        {
            $newUser = $this->populate($result[0]);
            return $newUser;
        }
        else
        {
            return false;
        }   
    }

    /**
     * insert User into storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   User $user
     * @return  User
     */
    public function insert(User $user)
    {
        try {

            $SQL = "INSERT IGNORE INTO Users
                    VALUES ('',
                            :RFID, 
                            :firstName, 
                            :lastName, 
                            :adress, 
                            :zipCode, 
                            :city, 
                            :dateOfBirth, 
                            :GSM, 
                            :mail, 
                            :password, 
                            :pin, 
                            :gender, 
                            NOW(),
                            :image,
                            NOW() + INTERVAL 1 YEAR
                            )
                    ";

            $statement = $this->getAdaptor()->getDb()->prepare($SQL);

            $params = array(':RFID' => $user->getRFID(),
                            ':firstName' => $user->getFirstName(),
                            ':lastName' => $user->getLastName(),
                            ':adress' => $user->getAdress(),
                            ':zipCode' => $user->getzipCode(),
                            ':city' => $user->getCity(),
                            ':dateOfBirth' => $user->getDateOfBirth(),
                            ':GSM' => $user->getGSM(),
                            ':mail' => $user->getMail(),
                            ':password' => $user->getPassword(),
                            ':pin' => $user->getPin(),
                            ':gender' => $user->getGender(),
                            ':image' => $user->getImage()
                );

            $statement->execute($params);
        }
        catch(PDOExeption $e) {
            throw $e;
        }

        if($statement->rowCount() == 1) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * Update User in storage
     *
     * @author  JasperDS <http://jasperdesmet.be>
     * @param   Article $article
     * @return  boolean
     */
    public function update(User $user)
    {
        
    }
}