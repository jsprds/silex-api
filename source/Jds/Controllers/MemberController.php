<?php

namespace Jds\Controllers;
 
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Silex\Application;
use \PDO;
use \PDOException;
 
class MemberController {

    // Return alle members in JSON formaat
    public function getAllMembers(Request $request, Application $app) {
    
        $GET_params =$request->query->all();

        if(!isset($GET_params['mail']) || !isset($GET_params['password'])) {
            $respons['status'] = 412;
            $respons['message'] = 'Paramater verwacht maar niet gevonden.';
        }
        else {
            try {
                // Authenticate requester
                if($app['MemberMapper']->authenticate($GET_params['mail'], $GET_params['password'], 1) === true) {
                    // Authentication gelukt

                    if(($members = $app['MemberMapper']->getAll()) !== false) {
                        $respons['status'] = 200;
                        $respons['message'] = '';

                        $membersArray = array();

                        foreach($members as $member) {
                            $memberArray = array();
                            $memberArray['id'] = $member->getId();
                            $memberArray['RFID'] = $member->getRFID();
                            $memberArray['firstname'] = $member->getFirstname();
                            $memberArray['lastname'] = $member->getLastname();
                            $memberArray['address'] = $member->getAddress();
                            $memberArray['zipCode'] = $member->getZipCode();
                            $memberArray['city'] = $member->getCity();
                            $memberArray['dateOfBirth'] = $member->getDateOfBirth();
                            $memberArray['GSM'] = $member->getGSM();
                            $memberArray['mail'] = $member->getMail();
                            //$memberArray['password'] = $member->getPassword();
                            //$memberArray['pin'] = $member->getPin();
                            $memberArray['gender'] = $member->getGender();
                            $memberArray['memberSince'] = $member->getMemberSince();
                            $memberArray['rfidExpireDate'] = $member->getRfidExpireDate();
                            $memberArray['image'] = $member->getImage();

                            $membersArray[] = $memberArray;
                        }

                        $respons['data'] = $membersArray;
                        
                    }
                    else {
                        $respons['status'] = 204;
                        $respons['message'] = 'Geen lid gevonden met id ' . $GET_params['memberId'];
                    }
                }
                else {
                    //Foute Credentials
                    $respons['status'] = 403;
                    $respons['message'] = 'Geen permissie';
                }
            }
            catch(PDOException $e) {
                $respons['status'] = 500;
                $respons['message'] = "SQL-error | code: " . $e->getCode();
                var_dump($e);
            }
        }
    
        return new Response(json_encode($respons));
    }    

    // Return één member in JSON
    public function getMemberById($memberId, Request $request, Application $app) {

        $GET_params =$request->query->all();

        if(!isset($GET_params['mail']) || !isset($GET_params['password'])) {
            $respons['status'] = 412;
            $respons['message'] = 'Paramater verwacht maar niet gevonden.';
        }
        else {
            try {
                // Authenticate requester
                if($app['MemberMapper']->authenticate($GET_params['mail'], $GET_params['password'], 1) === true) {
                    // Authentication gelukt

                    if(($member = $app['MemberMapper']->getById($memberId)) !== false) {
                        $respons['status'] = 200;
                        $respons['message'] = '';

                        $memberArray = array();
                        $memberArray['id'] = $member->getId();
                        $memberArray['RFID'] = $member->getRFID();
                        $memberArray['firstname'] = $member->getFirstname();
                        $memberArray['lastname'] = $member->getLastname();
                        $memberArray['address'] = $member->getAddress();
                        $memberArray['zipCode'] = $member->getZipCode();
                        $memberArray['city'] = $member->getCity();
                        $memberArray['dateOfBirth'] = $member->getDateOfBirth();
                        $memberArray['GSM'] = $member->getGSM();
                        $memberArray['mail'] = $member->getMail();
                        //$memberArray['password'] = $member->getPassword();
                        //$memberArray['pin'] = $member->getPin();
                        $memberArray['gender'] = $member->getGender();
                        $memberArray['memberSince'] = $member->getMemberSince();
                        $memberArray['rfidExpireDate'] = $member->getRfidExpireDate();
                        $memberArray['image'] = $member->getImage();

                        $respons['data'] = $memberArray;
                    }
                    else {
                        $respons['status'] = 204;
                        $respons['message'] = 'Geen lid gevonden met id ' . $memberId;
                    }
                }
                else {
                    //Foute Credentials
                    $respons['status'] = 403;
                    $respons['message'] = 'Geen permissie';
                }
            }
            catch(PDOException $e) {
                $respons['status'] = 500;
                $respons['message'] = "SQL-error | code: " . $e->getCode();
            }
        }
    
        return new Response(json_encode($respons));
    }

    // Delete member
    public function deleteMemberById($memberId, Request $request, Application $app) {
        $respons['status'] = 500;
        $respons['message'] = "Een member deleten is niet mogelijk.";

        return new Response(json_encode($respons));
    }

    // Create member
    public function createMember(Request $request, Application $app) {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            
            // Member opbject maken
            $member = $app['MemberMapper']->populate($data['member']);

            try {
                if(($duplicatedMember = $app['MemberMapper']->getByFirstnameLastnameDateOfBirth($member->getFirstname(), $member->getLastname(), $member->getDateOfBirth())) === false) {
                    $respons = $app['MemberMapper']->create($member);
                }
                else {
                    $respons['status'] = 409;
                    $respons['message'] = "Deze persoon is al geregistreerd in ons systeem";

                    $duplicatedMemberArray = array();
                    $duplicatedMemberArray['id'] = $member->getId();
                    $duplicatedMemberArray['RFID'] = $member->getRFID();
                    $duplicatedMemberArray['firstname'] = $member->getFirstname();
                    $duplicatedMemberArray['lastname'] = $member->getLastname();
                    $duplicatedMemberArray['address'] = $member->getAddress();
                    $duplicatedMemberArray['zipCode'] = $member->getZipCode();
                    $duplicatedMemberArray['city'] = $member->getCity();
                    $duplicatedMemberArray['dateOfBirth'] = $member->getDateOfBirth();
                    $duplicatedMemberArray['GSM'] = $member->getGSM();
                    $duplicatedMemberArray['mail'] = $member->getMail();
                    //$duplicatedMemberArray['password'] = $member->getPassword();
                    //$duplicatedMemberArray['pin'] = $member->getPin();
                    $duplicatedMemberArray['gender'] = $member->getGender();
                    $duplicatedMemberArray['memberSince'] = $member->getMemberSince();
                    $duplicatedMemberArray['rfidExpireDate'] = $member->getRfidExpireDate();
                    $duplicatedMemberArray['image'] = $member->getImage();

                    $respons['data']['duplicatedMember'] = $duplicatedMemberArray;
                }
            }
            catch(PDOException $e) {
                $respons['status'] = 500;
                $respons['message'] = "SQL-error | code: " . $e->getCode();
            }
        }
        else {
            $respons['status'] = 412;
            $respons['message'] = 'JSON verwacht maar niet gevonden.';
        }

        return new Response(json_encode($respons));
    }

    // Create member
    public function updateMember($memberId, Request $request, Application $app) {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            
            if(!isset($data['member']['id']) || $memberId != $data['member']['id']) {
                $respons['status'] = 500;
                $respons['message'] = "URl-id =! JSON-id";
            }
            else {
                // Member opbject maken
                $member = $app['MemberMapper']->populate($data['member']);
                $member->setId($memberId);

                try {
                    
                    $respons = $app['MemberMapper']->update($member);
                
                }
                catch(PDOException $e) {
                    $respons['status'] = 500;
                    $respons['message'] = "SQL-error | code: " . $e->getCode();
                }
            }
        }
        else {
            $respons['status'] = 412;
            $respons['message'] = 'JSON verwacht maar niet gevonden.';
        }

        return new Response(json_encode($respons));
    }
}