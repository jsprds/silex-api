<?php

	// Autoloader
	require_once __DIR__.'/../vendor/autoload.php';

	use Symfony\Component\Config\FileLocator;
	use Symfony\Component\Routing\Loader\YamlFileLoader;
	use Symfony\Component\Routing\RouteCollection;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Silex\Application;

	// Define app
	$app = new Application();

	// Please set to false in a production environment
	$app['debug'] = true;

	// FrontController --> regelt alle requesten
	$app['routes'] = $app->extend('routes', function (RouteCollection $routes, Application $app) {
	    
	    $loader = new YamlFileLoader(new FileLocator(__DIR__ . '/../app'));
	    $collection = $loader->load('routes.yml');
	    $routes->addCollection($collection);
	 
	    return $routes;
	});

	// Database configureren
	$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	    'db.options' => array(
	        'dbname' => 'sjatoo_ledenbeheer',
		'user' => 'root',
		'password' => '',
		'host' => 'localhost',
		'driver' => 'pdo_mysql',
	    ),
	));

	$app['PDOAdaptor'] = function () {
	    return new Jds\Models\PDOAdaptor('mysql:dbname=sjatoo_ledenbeheer;host=localhost','root','');
	};

	$app['MemberMapper'] = function ($app) {
	    return new Jds\Models\MemberMapper($app['PDOAdaptor']);
	};

	$app->after(function (Request $request, Response $response) {
		$response->headers->set('Content-Type', 'application/json');
	    $response->headers->set('Access-Control-Allow-Origin', '*');
	});

 
$app->run();